import Vue from 'vue'
import Router from 'vue-router'
import Chat from './pages/Chat.vue'
import Login from './pages/Login.vue'
import auth from 'firebase/auth'


Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'Chat',
      component: Chat,
      //method that run before user enter in this page
      beforeEnter: (to, from, next) => {
        // .currentUser()  is a firebase method
        // se l utente non e loggato vai alla pagina login
        if(!firebase.auth().currentUser) {
          next('/login')
        }else {
          next()
        }
      }
    },
    {
      path: '/login',
      name: 'Login ',
      component: Login
    },
    
  ]
})
