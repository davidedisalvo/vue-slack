import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import firebase from 'firebase/app'
import auth from 'firebase/auth'


Vue.config.productionTip = false

var config = {
  apiKey: "AIzaSyCK59jJKfKknnZeNeyfiqfOzpShrp2RG3M",
  authDomain: "vuexslack-5a65b.firebaseapp.com",
  databaseURL: "https://vuexslack-5a65b.firebaseio.com",
  projectId: "vuexslack-5a65b",
  storageBucket: "vuexslack-5a65b.appspot.com",
  messagingSenderId: "55011181690"
};
firebase.initializeApp(config);
window.firebase = firebase;
const unsuscribe = firebase.auth().onAuthStateChanged(user=>{ 
  store.dispatch('setUser', user)

  new Vue({
    router,
    store,
    render: h => h(App)
  }).$mount('#app')

  unsuscribe();
}) 


